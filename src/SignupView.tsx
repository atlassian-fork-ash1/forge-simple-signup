import ForgeUI, {
  Button,
  Fragment,
  Text,
  Table,
  Head,
  Row,
  Cell,
  Form,
  TextField,
  useProductContext
} from "@forge/ui";
import { useContentProperty } from "@forge/ui-confluence";
import { EntryData, FieldType } from "./types";
import api from "@forge/api";

interface SignupViewProps {
  fields: FieldType[];
  title: string;
  revertToConfig: () => void;
}

const SignupView = ({ fields, title, revertToConfig }: SignupViewProps) => {
  const { accountId } = useProductContext();
  const [entries, updateEntries] = useContentProperty<EntryData[]>(
    "entries",
    []
  );
  const hasSubmitted =
    entries.find(entry => entry.accountId === accountId) !== undefined;

  return (
    <Fragment>
      {hasSubmitted ? (
        <Fragment>
          <Text
            content={`You have signed up${title ? ` for ${title}` : ""}! ✅`}
          />
          <Button
            text="Remove yourself from this list ⛔️"
            onClick={async () => {
              await updateEntries(prev => {
                return prev.filter(row => row.accountId !== accountId);
              });
            }}
          />
        </Fragment>
      ) : fields.length > 0 ? (
        <Form
          onSubmit={async formData => {
            const data = await (
              await api.asUser().requestJira("/rest/api/3/myself")
            ).json();
            await updateEntries(prev => {
              return [
                ...prev,
                { ...formData, name: data.displayName, accountId }
              ];
            });
          }}
          submitButtonText={`Sign up ${title ? `for ${title} ` : ""}🎉`}
        >
          {fields.length > 0 && (
            <Text content="**Please fill in the following information before signing up**" />
          )}
          {fields.map(field => (
            <TextField name={field.label} label={field.label} />
          ))}
        </Form>
      ) : (
        <Button
          text={`Sign up ${title ? `for ${title} ` : ""}🎉`}
          onClick={async () => {
            const data = await (
              await api.asUser().requestJira("/rest/api/3/myself")
            ).json();
            await updateEntries(prev => {
              return [...prev, { name: data.displayName, accountId }];
            });
          }}
        />
      )}
      {entries.length > 0 && (
        <Fragment>
          <Table>
            <Head>
              <Cell>
                <Text content="**Name**" />
              </Cell>
              {fields.map(field => (
                <Cell>
                  <Text content={`**${field.label}**`} />
                </Cell>
              ))}
            </Head>
            {entries.map(entry => (
              <Row>
                <Cell>
                  <Text
                    content={`[${entry.name}](/wiki/people/${entry.accountId})`}
                  />
                </Cell>
                {fields.map(field => (
                  <Cell>
                    <Text content={entry[field.label] || ""} />
                  </Cell>
                ))}
              </Row>
            ))}
          </Table>
          <Button
            text="Edit Simple Sign-up 📝"
            onClick={() => {
              revertToConfig();
            }}
          />
        </Fragment>
      )}
    </Fragment>
  );
};

export default SignupView;
